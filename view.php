<?php
if (isset($_GET['id'])) {
	require_once('db_connect.php');

	try {

		$sql = 'SELECT * from list where id = :id';
		$s = $pdo->prepare($sql);
		$s->bindValue(':id', (int) $_GET['id']);
		$s->execute();

		$wes = $s->fetchObject();
	} catch (Exception $e) {
		echo 'Unable to select';
	}

} else {
	throw new Exception('ID param missed');
	die;
}
// var_dump($wes);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Подробности</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="stylesheet" href="main/main.css">
</head>
<body>
	<?php include 'header.php'?>
	<div class="container main">
		<div class="row">
			<div class="col">
				<h1 class="text-center">
					Подробное Описание
				</h1>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<ul class="text-center">
					<li>
						Номер ID:
						<?=$wes->id?>
					</li>
					<li>
						Ф.И.О:
						<?=$wes->fullName?>
					</li>
					<li>
						Телефон:
						<?=$wes->phone?>
					</li>
					<li>
						Email:
						<?=$wes->email?>
					</li>
					<li>
						Группа -
						<?=$wes->role?>
					</li>
					<?php if ($wes->role == 'Студент'): ?>
							Средняя оценка -
							<?=$wes->averangeMark?>
					<?php elseif ($wes->role == 'Администратор') : ?>
							Рабочий день -
							<?=$wes->workingDay?>
					<?php elseif ($wes->role == 'Преподователь'): ?>
							Предмет - 
							<?=$wes->subject?>
					<?php endif ?>								
				</ul>
				<div class="text-center">
					<a class="page-1" href="edit.php?id=<?=$wes->id?>">Edit</a>
					<a class="page-1" href="index.php">Back</a>
				</div>
			</div>
		</div>
	</div>
	<?php include 'footer.php' ?>
</body>
</html>