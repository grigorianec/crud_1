<?php

if (isset($_POST['fullName'])) {

	require_once('db_connect.php');
	

	try {
	$sql ='INSERT into list set
		fullName = :fullName,
		phone = :phone,
		email = :email,
		role = :role,
		averangeMark = :averangeMark,
		subject = :subject,
		workingDay = :workingDay
		';
	$s = $pdo->prepare($sql);

		$s->bindValue(':fullName', $_POST['fullName']);
		$s->bindValue(':phone', $_POST['phone']);
		$s->bindValue(':email', $_POST['email']);
		$s->bindValue(':role', $_POST['role']);
		$s->bindValue(':averangeMark', $_POST['averangeMark']);
		$s->bindValue(':subject', $_POST['subject']);
		$s->bindValue(':workingDay', $_POST['workingDay']);

		$s->execute();

	} catch (PDOException $e) {
		echo 'Cannot insert record';
	}

	header('Location: index.php');
	die;
	
}

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="stylesheet" href="main/main.css">
<body>
	<?php include 'header.php'?>
	<section>
		<div class="container">
			<div class="row">
				<div class="col">
					<h1 class="text-center">
						Заполните форму
					</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-6 mr-auto ml-auto">
					<form method="POST">
						<div class="form-group">
							<label for="exampleFormControlInput1">
								Ф.И.О
							</label>
							<input type="text"  name="fullName" class="form-control" id="exampleFormControlInput1" placeholder="Иванов Иван Иванович">
						</div>
						<div class="form-group">
							<label for="exampleFormControlInput1">
								Телефон
							</label>
							<input type="text" name="phone" class="form-control" id="exampleFormControlInput1" placeholder="+380934447834">
						</div>
						<div class="form-group">
							<label for="exampleFormControlInput1">
								Email address
							</label>
							<input type="email" name="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com">
						</div>
						<div class="form-group">
							<label for="exampleFormControlInput1">
								Средняя Оценка/Процент посещения
							</label>
							<input type="text" name="averangeMark" class="form-control" id="exampleFormControlInput1">
						</div>
						<div class="form-group">
							<label for="exampleFormControlInput1">
								Предмет
							</label>
							<input type="text" name="subject" class="form-control" id="exampleFormControlInput1" placeholder="PHP">
						</div>
						<div class="form-group">
							<label for="exampleFormControlInput1">
								Рабочий день
							</label>
							<input type="text" name="workingDay" class="form-control" id="exampleFormControlInput1" placeholder="Monday">
						</div>
						<div class="form-group">
							<label for="exampleFormControlSelect1">Example select</label>
							<select class="form-control" name="role" id="exampleFormControlSelect1">
								<option>
									Выберите группу
								</option>
								<option value="Студент">
									Студент
								</option>
								<option value="Администратор">
									Администратор
								</option>
								<option value="Преподователь">
									Преподователь
								</option>
							</select>
						</div>
						<button type="submit">
							Добавить
						</button>
					</form>
				</div>
			</div>
		</div>
	</section>
	<?php include 'footer.php' ?>
</body>
</html>